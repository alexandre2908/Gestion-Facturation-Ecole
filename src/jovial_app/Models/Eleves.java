package jovial_app.Models;

public class Eleves {
  private String matricule_eleve;
  private String nom_eleve;
  private String postnom;
  private String prenom;
  private String option_eleve;
  private String classe;
  private String section;

  public Eleves(String matricule_eleve, String nom_eleve, String postnom, String prenom, String option_eleve, String classe, String section) {
      this.matricule_eleve = matricule_eleve;
    this.nom_eleve = nom_eleve;
    this.postnom = postnom;
    this.prenom = prenom;
    this.option_eleve = option_eleve;
    this.classe = classe;
    this.section = section;
  }

  public String getMatricule_eleve() {
    return matricule_eleve;
  }

  public void setMatricule_eleve(String matricule_eleve) {
    this.matricule_eleve = matricule_eleve;
  }

  public String getNom_eleve() {
    return nom_eleve;
  }

  public void setNom_eleve(String nom_eleve) {
    this.nom_eleve = nom_eleve;
  }

  public String getPostnom() {
    return postnom;
  }

  public void setPostnom(String postnom) {
    this.postnom = postnom;
  }

  public String getPrenom() {
    return prenom;
  }

  public void setPrenom(String prenom) {
    this.prenom = prenom;
  }

  public String getOption_eleve() {
    return option_eleve;
  }

  public void setOption_eleve(String option_eleve) {
    this.option_eleve = option_eleve;
  }

  public String getClasse() {
    return classe;
  }

  public void setClasse(String classe) {
    this.classe = classe;
  }

  public String getSection() {
    return section;
  }

  public void setSection(String section) {
    this.section = section;
  }

  @Override
  public String toString() {
    return "Eleves{" +
            "matricule_eleve='" + matricule_eleve + '\'' +
            ", nom_eleve='" + nom_eleve + '\'' +
            ", postnom='" + postnom + '\'' +
            ", prenom='" + prenom + '\'' +
            ", option_eleve='" + option_eleve + '\'' +
            ", classe='" + classe + '\'' +
            ", section='" + section + '\'' +
            '}';
  }
}
