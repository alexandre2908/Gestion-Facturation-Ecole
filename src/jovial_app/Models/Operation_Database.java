package jovial_app.Models;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Operation_Database {
    Connection connection = Database_Connection.getconnection();

    public boolean Login_Verify(String username, String password) {
        boolean value = false;
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM utilisateurs WHERE username='" + username + "' AND password_user='" + password + "'");
            resultSet.last();
            int count = resultSet.getRow();
            if (count == 0) {
                value = false;
            } else {
                value = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return value;

    }

    public ResultSet getall_eleves() {
        ResultSet resultSet = null;
        try {
            Statement statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM eleves");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultSet;
    }
    public ResultSet getall_fiches() {
        ResultSet resultSet = null;
        try {
            Statement statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM fiche_perc");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultSet;
    }
    public ObservableList<String> getall_motif(){
        ObservableList<String>  list_motif= FXCollections.observableArrayList();
        ResultSet resultSet = null;
        try {
            Statement statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM frais");
            while (resultSet.next()){
                list_motif.add(resultSet.getString("libele"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list_motif;
    }
    public int insert_recus(Recus recus){
        int resultat = 0;
        int id_recus = 0;
        try {
            String sql = "INSERT INTO recus(montant_perc, libele_pay, date_percept, percepteur, reste_pay, id_eleve, id_frais) VALUES (?,?,?,?,?,?,?)";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setFloat(1,recus.getMontant_perc());
            statement.setString(2, recus.getLibele_paye());
            statement.setString(3, recus.getDate_percept());
            statement.setString(4, recus.getPercepeteur());
            statement.setFloat(5, recus.getReste_pay());
            statement.setInt(6, recus.getId_eleve());
            statement.setInt(7, recus.getId_frais());
            resultat = statement.executeUpdate();

            Statement statement1 = connection.createStatement();

            ResultSet resultSet = statement1.executeQuery("SELECT * FROM recus WHERE num_rec=(SELECT max(num_rec) FROM recus)");

            while (resultSet.next()){
                id_recus = resultSet.getInt("num_rec");
            }


        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id_recus;
    }

    public float get_montant(int id_frais){
        ResultSet resultSet = null;
        float montant = 0;
        try {
            Statement statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM frais WHERE id_frais="+id_frais);
            while (resultSet.next()){
                montant = resultSet.getFloat("montant");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return montant;
    }
    public String get_observation(int id_fiche){
        ResultSet resultSet = null;
        String observation = "";
        try {
            Statement statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT observation FROM fiche_perc WHERE num_fiche="+id_fiche);
            while (resultSet.next()){
                observation = resultSet.getString("observation");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return observation;
    }
    public int get_id_eleve(String matricule){
        ResultSet resultSet = null;
        int id_eleve = 0;
        try {
            Statement statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM eleves WHERE matricule_eleve='"+matricule+"'");
            while (resultSet.next()){
                id_eleve = resultSet.getInt("id_eleve");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id_eleve;
    }
    public int get_id_motif(String motif){
        ResultSet resultSet = null;
        int id_motif = 0;
        try {
            Statement statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT id_frais FROM frais WHERE libele='"+motif+"'");
            while (resultSet.next()){
                id_motif = resultSet.getInt("id_frais");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id_motif;
    }
    public float get_montant_motif_id_eleve(int id_motif, int id_eleve){
        ResultSet resultSet = null;
        float montant = 0;
        try {
            Statement statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT min(reste_pay) FROM recus WHERE id_frais="+id_motif+" AND id_eleve="+id_eleve);
            while (resultSet.next()){
                montant = resultSet.getFloat("reste_pay");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return montant;
    }

    public boolean verifie_fiche_today(){
        ResultSet resultSet = null;
        boolean retour = false;
        String date_ojourdhui = new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());

        try {
            Statement statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT count(*) AS nombre FROM fiche_perc WHERE date_perc='"+date_ojourdhui+"'");
            int resultat = 0;
            while (resultSet.next()){
                resultat = resultSet.getInt("nombre");
            }
            if (resultat > 0){
                retour = true;
            }else {
                retour  = false;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return retour;
    }
    public Fiche_perc get_dailyFiche(){
        ResultSet resultSet = null;
        Fiche_perc fiche_perc = new Fiche_perc();
        String date_ojourdhui = new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());

        try {
            Statement statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM fiche_perc WHERE date_perc='"+date_ojourdhui+"'");
            while (resultSet.next()){
                fiche_perc.setNum_fiche(resultSet.getInt("num_fiche"));
                fiche_perc.setDate_perc(resultSet.getString("date_perc"));
                fiche_perc.setMontant_percu(resultSet.getFloat("montant_percu"));
                fiche_perc.setObservation(resultSet.getString("observation"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return fiche_perc;
    }
    public int insert_fiche(Fiche_perc fiche_perc){
        int resultat = 0;
        try {
            String sql = "INSERT INTO fiche_perc(date_perc, montant_percu, observation) VALUES (?,?,?)";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, fiche_perc.getDate_perc());
            statement.setFloat(2,fiche_perc.getMontant_percu());
            statement.setString(3, fiche_perc.getObservation());
            resultat = statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultat;
    }
    public int insertFiche_detail(int num_fiche, int num_recu){
        int resultat = 0;
        try {
            String sql = "INSERT INTO detail_fiche(num_recu, num_fiche) VALUES (?,?)";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, num_recu);
            statement.setInt(2, num_fiche);
            resultat = statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultat;
    }
    public int update_montant_fiche(Fiche_perc fiche_perc){
        int resultat = 0;
        try {
            String sql = "UPDATE fiche_perc SET montant_percu="+fiche_perc.getMontant_percu()+" WHERE date_perc='"+fiche_perc.getDate_perc()+"'";
            Statement statement = connection.createStatement();
            resultat = statement.executeUpdate(sql);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultat;
    }
    public int update_observation_fiche(int id_fiche,String observation){
        int resultat = 0;
        try {
            String sql = "UPDATE fiche_perc SET observation=? WHERE num_fiche=?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, observation);
            statement.setInt(2, id_fiche);
            resultat = statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultat;
    }




}