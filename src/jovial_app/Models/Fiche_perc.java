package jovial_app.Models;

public class Fiche_perc {
  private int num_fiche;
  private String date_perc;
  private float montant_percu;
  private String observation;

  public Fiche_perc() {
  }

  public Fiche_perc(int num_fiche, String date_perc, float montant_percu, String observation) {
    this.num_fiche = num_fiche;
    this.date_perc = date_perc;
    this.montant_percu = montant_percu;
    this.observation = observation;
  }

  public int getNum_fiche() {
    return num_fiche;
  }

  public void setNum_fiche(int num_fiche) {
    this.num_fiche = num_fiche;
  }

  public String getDate_perc() {
    return date_perc;
  }

  public void setDate_perc(String date_perc) {
    this.date_perc = date_perc;
  }

  public float getMontant_percu() {
    return montant_percu;
  }

  public void setMontant_percu(float montant_percu) {
    this.montant_percu = montant_percu;
  }

  public String getObservation() {
    return observation;
  }

  public void setObservation(String observation) {
    this.observation = observation;
  }
}
