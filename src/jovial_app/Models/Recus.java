package jovial_app.Models;

public class Recus {
    private float montant_perc;
    private String libele_paye;
    private float reste_pay;
    private String date_percept;
    private String percepeteur;
    private int id_frais;
    private int id_eleve;

    public Recus() {
    }

    public Recus(float montant_perc, String libele_paye, float reste_pay, String date_percept, String percepeteur, int id_frais, int id_eleve) {
        this.montant_perc = montant_perc;
        this.libele_paye = libele_paye;
        this.reste_pay = reste_pay;
        this.date_percept = date_percept;
        this.percepeteur = percepeteur;
        this.id_frais = id_frais;
        this.id_eleve = id_eleve;
    }

    public float getMontant_perc() {
        return montant_perc;
    }

    public void setMontant_perc(float montant_perc) {
        this.montant_perc = montant_perc;
    }

    public String getLibele_paye() {
        return libele_paye;
    }

    public void setLibele_paye(String libele_paye) {
        this.libele_paye = libele_paye;
    }

    public float getReste_pay() {
        return reste_pay;
    }

    public void setReste_pay(float reste_pay) {
        this.reste_pay = reste_pay;
    }

    public String getDate_percept() {
        return date_percept;
    }

    public void setDate_percept(String date_percept) {
        this.date_percept = date_percept;
    }

    public String getPercepeteur() {
        return percepeteur;
    }

    public void setPercepeteur(String percepeteur) {
        this.percepeteur = percepeteur;
    }

    public int getId_frais() {
        return id_frais;
    }

    public void setId_frais(int id_frais) {
        this.id_frais = id_frais;
    }

    public int getId_eleve() {
        return id_eleve;
    }

    public void setId_eleve(int id_eleve) {
        this.id_eleve = id_eleve;
    }
}
