package jovial_app.Controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import jovial_app.Models.Operation_Database;
import jovial_app.utils.Metadata;
import jovial_app.utils.Windows_Opening;

import java.net.URL;
import java.util.ResourceBundle;

public class login implements Initializable{
    public JFXTextField username;
    public JFXButton btn_login;
    public Label btn_fermer;
    public JFXPasswordField password;
    public Label retour_connect;

    Windows_Opening windows_opening = new Windows_Opening();
    Operation_Database operation_database = new Operation_Database();

    /**
     * Called to initialize a controller after its root element has been
     * completely processed.
     *
     * @param location  The location used to resolve relative paths for the root object, or
     *                  <tt>null</tt> if the location is not known.
     * @param resources The resources used to localize the root object, or <tt>null</tt> if
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        btn_fermer.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                System.exit(0);
            }
        });
        btn_login.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                boolean login_test = operation_database.Login_Verify(username.getText(), password.getText());
                if (login_test){
                    Metadata.username = username.getText();
                    windows_opening.OpenNew_Full_Windows(event,resources,"Main_Window","Main Window");
                }else {
                    retour_connect.setText("Nom d'utilisateur ou Mots de pass incorrect");
                }
            }
        });
    }
}
