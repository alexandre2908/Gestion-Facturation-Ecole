package jovial_app.Controllers;

import com.jfoenix.controls.JFXButton;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import jovial_app.Models.Eleves;
import jovial_app.Models.Fiche_perc;
import jovial_app.Models.Operation_Database;
import jovial_app.utils.Metadata;
import jovial_app.utils.Windows_Opening;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.ResourceBundle;

public class MainWindow  implements Initializable{
    public Label username_recup;
    public Label date_today;
    public TableView<Eleves> tableView;
    public TableColumn<Eleves, String> matricule;
    public TableColumn<Eleves, String> nom;
    public TableColumn<Eleves, String> postnom;
    public TableColumn<Eleves, String> prenom;
    public TableColumn<Eleves, String> classe;
    public TableColumn<Eleves, String> option;
    public TableColumn<Eleves, String> section;
    public TableView fiche_table;
    public TableColumn numfiche;
    public TableColumn montant_perc;
    public TableColumn observation;
    public TableColumn date_perc;
    public Tab tab_fiches;
    public Tab tab_elevs;
    public JFXButton btn_add_eleves;

    private ObservableList<Eleves> eleves_listes = FXCollections.observableArrayList();
    private ObservableList<Fiche_perc> fiches_listes = FXCollections.observableArrayList();
    private Operation_Database operation_database = new Operation_Database();
    private Windows_Opening windows_opening = new Windows_Opening();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        username_recup.setText(Metadata.username);
        String timeStamp = new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());
        date_today.setText(timeStamp);
        initCol_eleves();
        initCol_Fiche();

        loadData_fiche();
        loadData_eleves();
        tab_fiches.setOnSelectionChanged(new EventHandler<Event>() {
            @Override
            public void handle(Event event) {
                loadData_fiche();
                loadData_eleves();
            }
        });
        tab_elevs.setOnSelectionChanged(new EventHandler<Event>() {
            @Override
            public void handle(Event event) {
                loadData_fiche();
                loadData_eleves();
            }
        });

        tableView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    Eleves eleves = tableView.getSelectionModel().getSelectedItem();
                    Metadata.eleves = eleves;
                    windows_opening.OpenNew_FloatingWindow(event, resources, "Intermediare","intermediaire");
                    //System.out.println(eleves);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        fiche_table.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    Fiche_perc fiche_perc = (Fiche_perc) fiche_table.getSelectionModel().getSelectedItem();
                    Metadata.fiche_perc = fiche_perc;
                    windows_opening.OpenNew_FloatingWindow(event, resources, "observation","intermediaire");
                    loadData_fiche();
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }
        });
        btn_add_eleves.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                windows_opening.OpenNew_FloatingWindow(event, resources, "ajouterEleves","Ajouter Eleves");
            }
        });

    }

    private void initCol_eleves() {
        matricule.setCellValueFactory(new PropertyValueFactory<>("matricule_eleve"));
        nom.setCellValueFactory(new PropertyValueFactory<>("nom_eleve"));
        postnom.setCellValueFactory(new PropertyValueFactory<>("postnom"));
        prenom.setCellValueFactory(new PropertyValueFactory<>("prenom"));
        classe.setCellValueFactory(new PropertyValueFactory<>("classe"));
        option.setCellValueFactory(new PropertyValueFactory<>("option_eleve"));
        section.setCellValueFactory(new PropertyValueFactory<>("section"));
    }
    private void initCol_Fiche() {
        numfiche.setCellValueFactory(new PropertyValueFactory<>("num_fiche"));
        montant_perc.setCellValueFactory(new PropertyValueFactory<>("montant_percu"));
        date_perc.setCellValueFactory(new PropertyValueFactory<>("date_perc"));
        observation.setCellValueFactory(new PropertyValueFactory<>("observation"));
    }

    private void loadData_eleves() {
        eleves_listes.clear();

        ResultSet resultSet = operation_database.getall_eleves();
        try {
            while (resultSet.next()) {
                String matricule = resultSet.getString("matricule_eleve");
                String nom_eleve = resultSet.getString("nom_eleve");
                String postnom = resultSet.getString("postnom");
                String prenom = resultSet.getString("prenom");
                String classe = resultSet.getString("classe");
                String option_eleve = resultSet.getString("option_eleve");
                String section = resultSet.getString("section");

                eleves_listes.add(new Eleves(matricule, nom_eleve, postnom, prenom, option_eleve, classe, section));

            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        tableView.setItems(eleves_listes);
    }
    private void loadData_fiche() {
        fiches_listes.clear();

        ResultSet resultSet = operation_database.getall_fiches();
        try {
            while (resultSet.next()) {
                int numero_fiche = resultSet.getInt("num_fiche");
                String date_de_perc = resultSet.getString("date_perc");
                String observation_db = resultSet.getString("observation");
                float montant_jr = resultSet.getFloat("montant_percu");

                fiches_listes.add(new Fiche_perc(numero_fiche, date_de_perc, montant_jr, observation_db));

            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        fiche_table.setItems(fiches_listes);
    }
}
