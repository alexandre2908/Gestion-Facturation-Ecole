package jovial_app.Controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import jovial_app.Models.Eleves;
import jovial_app.Models.Fiche_perc;
import jovial_app.Models.Operation_Database;
import jovial_app.Models.Recus;
import jovial_app.utils.Metadata;
import jovial_app.utils.Windows_Opening;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.ResourceBundle;

public class EnregistrerRecu implements Initializable{

    public Label date_today;
    public Label nom_complet;
    public TextArea libele_paie;
    public ComboBox motif_paie;
    public JFXTextField montant_paie;
    public Label reste_paie;
    public JFXButton btn_save;
    public JFXButton btn_cancel;
    private Operation_Database operation_database = new Operation_Database();

    /**
     * Called to initialize a controller after its root element has been
     * completely processed.
     *
     * @param location  The location used to resolve relative paths for the root object, or
     *                  <tt>null</tt> if the location is not known.
     * @param resources The resources used to localize the root object, or <tt>null</tt> if
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        String date_ojourdhui = new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());
        date_today.setText(date_ojourdhui);
        Eleves eleves = Metadata.eleves;
        nom_complet.setText(eleves.getNom_eleve()+" "+ eleves.getPostnom()+" "+eleves.getPrenom());
        initcombobox();
        motif_paie.getSelectionModel().select(0);
        reste_paie.setText(String.valueOf(operation_database.get_montant_motif_id_eleve(1,getid_eleve(Metadata.eleves.getMatricule_eleve()))));
        btn_cancel.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ((Node)(event.getSource())).getScene().getWindow().hide();
            }
        });
        btn_save.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                int id_frais = motif_paie.getSelectionModel().getSelectedIndex() + 1;
                Float montant = Float.valueOf(montant_paie.getText());
                String libele = libele_paie.getText();
                Float reste_pay =getreste_pay(id_frais, montant);
                int id_eleves = getid_eleve(Metadata.eleves.getMatricule_eleve());
                Recus recus = new Recus(montant, libele, reste_pay, date_ojourdhui, Metadata.username, id_frais, id_eleves);
                if (operation_database.verifie_fiche_today()){
                    int id_recus = operation_database.insert_recus(recus);
                    Fiche_perc fiche_perc =operation_database.get_dailyFiche();
                    fiche_perc.setMontant_percu(fiche_perc.getMontant_percu() + montant);
                    operation_database.update_montant_fiche(fiche_perc);
                    operation_database.insertFiche_detail(fiche_perc.getNum_fiche(), id_recus);
                    Windows_Opening.close_window(event);

                }else {
                    Fiche_perc fiche_perc = new Fiche_perc();
                    fiche_perc.setDate_perc(date_ojourdhui);
                    operation_database.insert_fiche(fiche_perc);
                    int id_recus = operation_database.insert_recus(recus);
                    Fiche_perc today_fiche =operation_database.get_dailyFiche();
                    today_fiche.setMontant_percu(fiche_perc.getMontant_percu() + montant);
                    operation_database.update_montant_fiche(today_fiche);
                    operation_database.insertFiche_detail(today_fiche.getNum_fiche(), id_recus);
                    Windows_Opening.close_window(event);

                }
            }
        });
        motif_paie.valueProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                int id_motif = operation_database.get_id_motif(newValue.toString());
                int id_eleves = getid_eleve(Metadata.eleves.getMatricule_eleve());
                float montant_restant = operation_database.get_montant_motif_id_eleve(id_motif, id_eleves);
                if (montant_restant <= 0.0){
                    reste_paie.setText(String.valueOf(operation_database.get_montant(id_motif)));
                }else {
                    reste_paie.setText(String.valueOf(montant_restant));
                }
            }
        });
    }

    private int getid_eleve(String matricule_eleve) {
        int id_eleve = operation_database.get_id_eleve(matricule_eleve);
        return id_eleve;
    }

    private void initcombobox() {
        ObservableList<String>  list_motif = operation_database.getall_motif();
        motif_paie.setItems(list_motif);
    }
    private Float getreste_pay(int id_frais, float montant_percu){
        float montant_frais = operation_database.get_montant(id_frais);

        return montant_frais-montant_percu;
    }
}
