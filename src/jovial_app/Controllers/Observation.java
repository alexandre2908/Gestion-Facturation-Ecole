package jovial_app.Controllers;

import com.jfoenix.controls.JFXButton;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.TextField;
import jovial_app.Models.Eleves;
import jovial_app.Models.Fiche_perc;
import jovial_app.Models.Operation_Database;
import jovial_app.utils.Metadata;
import jovial_app.utils.Windows_Opening;

import java.net.URL;
import java.util.ResourceBundle;

public class Observation implements Initializable{
    public JFXButton btn_save;
    public JFXButton btn_close;
    public TextField txt_observation;

    private ObservableList<Eleves> eleves_listes = FXCollections.observableArrayList();
    private ObservableList<Fiche_perc> fiches_listes = FXCollections.observableArrayList();

    private Operation_Database operation_database = new Operation_Database();
    private Windows_Opening windows_opening = new Windows_Opening();

    /**
     * Called to initialize a controller after its root element has been
     * completely processed.
     *
     * @param location  The location used to resolve relative paths for the root object, or
     *                  <tt>null</tt> if the location is not known.
     * @param resources The resources used to localize the root object, or <tt>null</tt> if
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        int id_fiche = Metadata.fiche_perc.getNum_fiche();
        String observation = operation_database.get_observation(id_fiche);
        txt_observation.setText(observation);

        btn_close.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ((Node)(event.getSource())).getScene().getWindow().hide();
            }
        });

        btn_save.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String observation = txt_observation.getText();
                operation_database.update_observation_fiche(Metadata.fiche_perc.getNum_fiche(), observation);
                ((Node)(event.getSource())).getScene().getWindow().hide();
            }
        });
    }
}
