package jovial_app.Controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import jovial_app.Models.Operation_Database;
import jovial_app.utils.Windows_Opening;

import java.net.URL;
import java.util.ResourceBundle;

public class AjouterEleves implements Initializable{
    public JFXTextField txtnom_eleve;
    public JFXTextField txtpostnom_eleve;
    public JFXTextField txtclasse_eleve;
    public JFXTextField txtprenom_eleve;
    public JFXTextField txtdate_eleve;
    public JFXButton btn_ajouter;
    public JFXButton btn_annule;
    public Label txt_retour;


    private Operation_Database operation_database = new Operation_Database();
    private Windows_Opening windows_opening = new Windows_Opening();
    /**
     * Called to initialize a controller after its root element has been
     * completely processed.
     *
     * @param location  The location used to resolve relative paths for the root object, or
     *                  <tt>null</tt> if the location is not known.
     * @param resources The resources used to localize the root object, or <tt>null</tt> if
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btn_annule.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Windows_Opening.close_window(event);
            }
        });

    }
}
