package jovial_app.Controllers;

import com.jfoenix.controls.JFXButton;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import jovial_app.utils.Windows_Opening;

import java.net.URL;
import java.util.ResourceBundle;

public class Intermediare implements Initializable{

    public JFXButton btn_save;
    public JFXButton btn_close;
    private Windows_Opening windows_opening = new Windows_Opening();

    /**
     * Called to initialize a controller after its root element has been
     * completely processed.
     *
     * @param location  The location used to resolve relative paths for the root object, or
     *                  <tt>null</tt> if the location is not known.
     * @param resources The resources used to localize the root object, or <tt>null</tt> if
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btn_close.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ((Node)(event.getSource())).getScene().getWindow().hide();
            }
        });

        btn_save.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                windows_opening.OpenNew_FloatingWindowAndClose(event, resources,"Enregistrer_Recu", "Enregistrer");
            }
        });
    }
}
